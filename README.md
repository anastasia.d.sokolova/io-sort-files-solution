# io-sort-files


## Description

In this task you need to implement [src/main/java/merge/MergeIOFactory.java](https://gitlab.com/anastasia.d.sokolova/io-sort-files/-/blob/master/src/main/java/merge/MergeIOFactory.java) method.
This method has 3 arguments:

- **inputFiles** - is a list of input files (one or many), where numbers and words are stored. <br />
- **outputIntegers** - is an output file, that should store numbers. <br />
- **outputStrings** - is an output file, that should store words.

Method must read data from input files, divide it into Integers and Strings, sort those groups and put sorted data into output files.

***Integer*** *- is a wrapper around the simple data type int. It takes values from  -2147483648 to 214748364.* <br />
***String*** *- is a class that represents character strings. It might be characters, as well as words.*

## Example

Given 2 input files:

#### input1
    i 20 love programming 
    40

#### input2
    50 -4 0 cool

After using mergeFiles method we should get 2 output files:

#### outputIntegers
    -4 
    0 
    20 
    40 
    50 

#### outputStrings
    cool
    i
    love
    programming


