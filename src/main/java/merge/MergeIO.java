package merge;

import java.io.File;
import java.util.List;

public interface MergeIO {

        void mergeFiles(List<File> inputFiles, File outputIntegers, File outputStrings);
}
