package solution;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class FileService {

    public FileService() {
    }

    public ArrayList<LinkedList<String>> parseFilesToArrs(List<File> files){
        ArrayList<LinkedList<String>> arrs = new ArrayList();
        LinkedList<String> arrStrings = new LinkedList<>();
        LinkedList<String> arrIntegers = new LinkedList<>();
        for (int i = 0; i < files.size(); i++) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(files.get(i)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String str;
            try {
                while ((str = reader.readLine()) != null) {
                    String[] words = str.split("\\s");
                    for (int j = 0; j < words.length; j++) {
                        try {
                            Integer.parseInt(words[j]);
                            arrIntegers.add(words[j]);
                        } catch (NumberFormatException e) {
                            arrStrings.add(words[j]);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        arrs.add(arrIntegers);
        arrs.add(arrStrings);
        return arrs;
    }


    public void parseArrsToFiles(LinkedList<String> arr, File output) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(output));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            for (String s : arr) {
                writer.write(s);
                writer.write("\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
