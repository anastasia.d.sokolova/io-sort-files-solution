package solution;

import merge.MergeIO;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MergeService implements MergeIO {
    FileService fs;
    MergeSort mergeSort;
    public void mergeFiles(List<File> inputFiles, File outputInt, File outputStr) {
        fs = new FileService();
        mergeSort = new MergeSort();
        ArrayList<LinkedList<String>> arr = fs.parseFilesToArrs(inputFiles);

        mergeSort.sort(arr.get(0), "Integer");
        mergeSort.sort(arr.get(1), "String");

        fs.parseArrsToFiles(arr.get(0), outputInt);
        fs.parseArrsToFiles(arr.get(1), outputStr);
    }
}
