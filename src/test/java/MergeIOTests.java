import merge.MergeIO;
import merge.MergeIOFactory;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


public class MergeIOTests {
    MergeIO factory;
    File intOutput = new File("src/test/resources/outputs/integerOutput");
    File strOutput = new File("src/test/resources/outputs/stringOutput");


    @BeforeEach
    public void init() {
        factory = new MergeIOFactory().mergeIO();
    }

    @Test
    public void mergeOneFileTest() {
        ArrayList<File> inputs = new ArrayList<>();
        inputs.add(new File("src/test/resources/OneInputTest/input1"));
        factory.mergeFiles(inputs, intOutput, strOutput);

        try {
            Assertions.assertTrue(FileUtils.contentEquals(intOutput, new File("src/test/resources/OneInputTest/integerOutputExpect")));
            Assertions.assertTrue(FileUtils.contentEquals(strOutput, new File("src/test/resources/OneInputTest/stringOutputExpect")));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void mergeTwoFilesTest() {
        ArrayList<File> inputs = new ArrayList<>();
        inputs.add(new File("src/test/resources/TwoInputTest/input1"));
        inputs.add(new File("src/test/resources/TwoInputTest/input2"));
        factory.mergeFiles(inputs, intOutput, strOutput);

        try {
            Assertions.assertTrue(FileUtils.contentEquals(intOutput, new File("src/test/resources/TwoInputTest/integerOutputExpect")));
            Assertions.assertTrue(FileUtils.contentEquals(strOutput, new File("src/test/resources/TwoInputTest/stringOutputExpect")));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void mergeManyFilesTest() {
        ArrayList<File> inputs = new ArrayList<>();
        inputs.add(new File("src/test/resources/ManyInputTest/input1"));
        inputs.add(new File("src/test/resources/ManyInputTest/input2"));
        inputs.add(new File("src/test/resources/ManyInputTest/input3"));
        factory.mergeFiles(inputs, intOutput, strOutput);

        try {
            Assertions.assertTrue(FileUtils.contentEquals(intOutput, new File("src/test/resources/ManyInputTest/integerOutputExpect")));
            Assertions.assertTrue(FileUtils.contentEquals(strOutput, new File("src/test/resources/ManyInputTest/stringOutputExpect")));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void mergeOnlyNumbersTest() {
        ArrayList<File> inputs = new ArrayList<>();
        inputs.add(new File("src/test/resources/OnlyIntegersTest/input1"));
        inputs.add(new File("src/test/resources/OnlyIntegersTest/input2"));
        factory.mergeFiles(inputs, intOutput, strOutput);

        try {
            Assertions.assertTrue(FileUtils.contentEquals(intOutput, new File("src/test/resources/OnlyIntegersTest/integerOutputExpect")));
            Assertions.assertTrue(FileUtils.contentEquals(strOutput, new File("src/test/resources/OnlyIntegersTest/stringOutputExpect")));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void mergeOnlyStringsTest() {
        ArrayList<File> inputs = new ArrayList<>();
        inputs.add(new File("src/test/resources/OnlyStringsTest/input1"));
        inputs.add(new File("src/test/resources/OnlyStringsTest/input2"));
        factory.mergeFiles(inputs, intOutput, strOutput);

        try {
            Assertions.assertTrue(FileUtils.contentEquals(intOutput, new File("src/test/resources/OnlyStringsTest/integerOutputExpect")));
            Assertions.assertTrue(FileUtils.contentEquals(strOutput, new File("src/test/resources/OnlyStringsTest/stringOutputExpect")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}
